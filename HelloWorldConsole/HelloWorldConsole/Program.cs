﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tulostaa tekstin Hello World
            Console.WriteLine("Hello Git");

            Laskin laskin1 = new Laskin();
            int summa = laskin1.Summa(5, 6);
            Console.WriteLine("Summa: ");

            int summa2 = laskin1.Summa(3, 12);
            Console.WriteLine("Summa: ");

            int summa3 = laskin1.Summa(4, 12);
            Console.WriteLine("Summa: ");

        }
    }
}
